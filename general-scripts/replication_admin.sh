#!/bin/bash

ACTION=$1

case "${ACTION}" in
	"on")
		TRANSPORT="transport-user"
		FLUSH="true"
		STRING="activation"
		;;
	"off")
		TRANSPORT="transport-user-off"
		FLUSH="false"
		STRING="deactivation"
		;;
	*)
		echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] USAGE: replication_admin.sh (on|off)"
		exit 1
esac

REPL_URL="https://localhost:5443/etc/replication/agents.author/publish02-1/jcr:content https://localhost:5443/etc/replication/agents.author/publish02-2/jcr:content"
FLUSH_URL="https://localhost:5443/etc/replication/agents.author/flush02-1/jcr:content https://localhost:5443/etc/replication/agents.author/flush02-2/jcr:content"
USER="m.consolazio"
PASSWORD="C8OhSv4boYdqZpA"

curl -s -k -u ${USER}:${PASSWORD} -X POST --data transportUser=${TRANSPORT} ${REPL_URL} > /dev/null
if [ $? -ne 0 ]; then
	echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] Error during Replication Agents ${STRING}."
	exit 1
else
	echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Replication Agents ${STRING} correctly done."
fi

curl -s -k -u ${USER}:${PASSWORD} -X POST --data enabled=${FLUSH} ${FLUSH_URL} > /dev/null
if [ $? -ne 0 ]; then
	echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] Error during Flush Agents ${STRING}."
	exit 1
else
	echo "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Flush Agents ${STRING} correctly done."
fi

exit 0
