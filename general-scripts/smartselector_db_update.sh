#!/bin/bash

ENV=$1

case "${ENV}" in
        "qa")
                ENDPOINT=st-cloud-qa-rds.cklyyd2yd4vl.eu-west-1.rds.amazonaws.com
                BD_PASS='fdj1tNf4qalV3WVgkEnb'
                ;;
        "preprod")
                ENDPOINT=st-cloud-preprod-rds.cklyyd2yd4vl.eu-west-1.rds.amazonaws.com
                BD_PASS='EtX"@jD"{73:y8-F'
                ;;
        "prod")
                ENDPOINT=st-cloud-rds-prod-01.cklyyd2yd4vl.eu-west-1.rds.amazonaws.com
                BD_PASS='kplaZ5uzoQeu92qM7Fo1'
                ;;
        *)
                echo "$(date '+%Y-%m-%d %H:%M') [ERROR] USAGE: bash smartselector_db_update.sh *env*"
                exit 1
esac

BUCKET_IMPORT=s3://st-jboss/smartselector/import-files/${ENV}/$(date '+%Y%m%d')/
BUCKET_DUMP=s3://st-jboss/smartselector/old-dumps/${ENV}/

BKP_DIR=/tmp/DUMP_$(date '+%Y%m%d_%H%M%S')/
BKP_NAME=$(date '+%Y%m%d_%H%M')-smartselectors_${ENV}_dump.sql.gz

echo "############################################"
echo "############### JOB STARTING ###############"
echo "############################################"

# Create the backup directory into tmp folder to download files in it
mkdir ${BKP_DIR}

echo "$(date '+%Y-%m-%d %H:%M') [INFO] Creating Dump of DB..."

# Perform a dump of the DB
mysqldump --force -usmart_${ENV} -p${BD_PASS} -h ${ENDPOINT} smartselectors_${ENV} | gzip -c > ${BKP_DIR}${BKP_NAME}

# Check if the dump gone well
if [ ${PIPESTATUS[0]} -ne 0  -o ! -a ${BKP_DIR}${BKP_NAME} ]; then
        echo "$(date '+%Y-%m-%d %H:%M') [ERROR] Error during the creation of dump"
        exit 1
else
	echo "$(date '+%Y-%m-%d %H:%M') [INFO] Dump correctly created"
fi

echo "$(date '+%Y-%m-%d %H:%M') [INFO] Uploading dump to S3..."

# Upload the Dump on S3
aws s3 cp ${BKP_DIR}${BKP_NAME} ${BUCKET_DUMP} --quiet

# Check if the upload gone well
if [ $? -ne 0 ]; then
        echo "$(date '+%Y-%m-%d %H:%M') [ERROR] Error during upload of Dump file to S3"
        exit 1
else
        echo "$(date '+%Y-%m-%d %H:%M') [INFO] Dump files correctly uploaded to S3"
fi

echo "$(date '+%Y-%m-%d %H:%M') [INFO] Downloading import files from S3..."

# Synchronyze the content of the today's folder in the temp folder
aws s3 sync ${BUCKET_IMPORT} ${BKP_DIR} --quiet

# Check if both files exists in the temp folder
if [ ! -f ${BKP_DIR}/Dump_T1.sql  -o ! -a ${BKP_DIR}/Dump_V1.sql -o $? -ne 0 ]; then
        echo "$(date '+%Y-%m-%d %H:%M') [ERROR] Error during the download of import files"
	exit 1
fi

echo "$(date '+%Y-%m-%d %H:%M') [INFO] Importing Tables..."

# Import the Tables of the SQL file
#mysql -usmart_${ENV} -D smartselectors_${ENV} -h ${ENDPOINT} < ${BKP_DIR}Dump_T1.sql

#Check if Tables are imported
if [ $? -ne 0 ]; then
        echo "$(date '+%Y-%m-%d %H:%M') [ERROR] Error during import of Tables"
	exit 1
else
        echo "$(date '+%Y-%m-%d %H:%M') [INFO] Tables correctly imported"
fi

echo "$(date '+%Y-%m-%d %H:%M') [INFO] Importing Views..."

# Import the Views of the SQL file
#mysql -usmart_${ENV} -D smartselectors_${ENV} -h ${ENDPOINT} < ${BKP_DIR}Dump_V1.sql

#Check if Views are imported
if [ $? -ne 0 ]; then
        echo "$(date '+%Y-%m-%d %H:%M') [ERROR] Error during import of Views"
        exit 1
else
        echo "$(date '+%Y-%m-%d %H:%M') [INFO] Views correctly imported"
fi

# Remove the temp folder created before
rm -rf ${BKP_DIR}

echo "############################################"
echo "############### JOB FINISHED ###############"
echo "############################################"

exit 0
