#!/usr/bin/python

import sys
import boto3
from datetime import datetime, timedelta
from geoip import geolite2
from geoip import open_database

def main(region,hour_retention,site):
    db = open_database('GeoLite2-Country.mmdb')
    already_checked = []
    csv_file = site + "_" + region + "_ip_tracking_" + str(datetime.now().strftime('%Y%m%d_%H%M%S')) + "_" + str(hour_retention) + "h.csv"
    s3_bucket = "st-microelectronics-sw/ip_tracker"

    if site == "www":
        log_file = "/etc/httpd/logs/www.st.com_http_access.log"
    else:
        log_file = "/etc/httpd/logs/my.st.com_https_access.log"

    ap_region = ["BN","KH","TL","ID","LA","MY","MM","PH","SG","TH","VN","HK","MO","JP","MN","KP","KR","TW","AS","PF","PN","WS","TO","TV","WF","AU","CX","CC","NZ","CK","NU","TK","FJ","NC","PG","SB","VU","FM","GU","KI","MH","NR","MP","PW","BD","BT","IO","IN","MV","NP","PK","LK"]
    na_region = ["AI","AG","AW","BS","BB","BZ","BM","VG","CA","KY","CR","CU","CW","DM","DO","SV","GL","GD","GT","HT","HN","JM","MX","MS","NI","PA","PR","BL","KN","LC","MF","PM","VC","TT","TC","US","VI"]
    cn_region = ["CN"]

    if region == "ap":
        array_region = ap_region
    elif (region == "cn01") or (region == "cn02") or (region == "cn03"):
        array_region = cn_region
    elif region == "na":
        array_region = na_region
    else:
        array_region = region

    out_file = open(csv_file,"w")
    
    data_raw = datetime.now() - timedelta(hours = int(hour_retention))
    data_retention_str = data_raw.strftime('%d/%b/%Y:%H:%M:%S')
    data_retention = datetime.strptime(data_retention_str, '%d/%b/%Y:%H:%M:%S')

    print str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " INFO: Job starting. Retention: " + str(hour_retention) + " hour/s"

    count_ip = 0
    count_ok = 0
    count_ko = 0
    with open(log_file) as f:
        for line in f:
            data_log_str =  find_between(line,"["," +0200")
            data_log = datetime.strptime(data_log_str, '%d/%b/%Y:%H:%M:%S')

            if data_retention > data_log:
               continue 

            csv_line = ""
            ip = line.split(' ')[0]
            if (ip != "-") and (ip not in already_checked) and not (ip.startswith('10.')):
                already_checked.append(ip)
                match = geolite2.lookup(ip)                               
                if hasattr(match, 'country'):
                    print str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " INFO: Checking " + ip
                    country = match.country
                    count_ip += 1
                    csv_line = csv_line + ip + "," + country

                    if (array_region == "eu01") or (array_region == "eu02") or (array_region == "eu03"):
                        if (country not in ap_region) and (country not in na_region) and (country not in cn_region):
                            count_ok += 1
                            csv_line = csv_line + "," + "OK"
                        else:
                            count_ko += 1
                            csv_line = csv_line + "," + "WRONG"
                    else:
                        if country in array_region:
                            count_ok += 1
                            csv_line = csv_line + "," + "OK"
                        else:
                            count_ko += 1
                            csv_line = csv_line + "," + "WRONG"

                    out_file.write(csv_line + "\n")

    failure_percent = int(count_ko * 1.0 / count_ip * 100)
    
    last_row = "TOT:" + str(count_ip) + ",OK:" + str(count_ok) + ",KO:" + str(count_ko) + ",FAIL:" + str(failure_percent) + "%"

    print last_row
    out_file.write(last_row)
    out_file.close()

    s3 = boto3.resource('s3')
    data = open(csv_file, 'rb')
    s3.Bucket(s3_bucket).put_object(Key=csv_file, Body=data)

    print str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + " INFO: Job finished."

def find_between(s,first,last):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

if __name__ == '__main__':
    region = sys.argv[1]
    hour_retention = sys.argv[2]
    site = sys.argv[3]
    main(region,hour_retention,site)
