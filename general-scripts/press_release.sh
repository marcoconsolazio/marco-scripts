#!/bin/bash

PDF_NAME=$1
#SERVERS=("10.59.2.10" "10.59.2.90" "10.59.2.170" "10.29.3.116" "10.85.3.161" "10.10.1.80" "10.10.11.139" "10.10.11.146")
SERVERS=("10.59.6.10" "10.59.6.125")

ST_COM_DIR=/mnt/disk-b/aem/cache/content/st_com/
MEDIA_DIR=${ST_COM_DIR}en/about/media-center/

USER="rundeck"
PEM="/var/lib/rundeck/.ssh/rundeckuser.pem"
AUX_PRESS="aux_press.sh"

NOW_DATE=$(date '+%Y%m%d')
S3_BUCKET=s3://st-storm-internal/press-release/${NOW_DATE}
S3_PROFILE=stormS3

echo "sudo -i
mkdir -p ${MEDIA_DIR}
cd ${ST_COM_DIR}
yes | cp en.html en.html_bkp
cd ${MEDIA_DIR}
yes | cp press-and-news.html press-and-news.html_bkp
yes | aws s3 cp ${S3_BUCKET}/en.html ${ST_COM_DIR} --profile ${S3_PROFILE}
yes | aws s3 cp ${S3_BUCKET}/press-and-news.html ${MEDIA_DIR} --profile ${S3_PROFILE}
aws s3 cp ${S3_BUCKET}/${PDF_NAME} ${MEDIA_DIR} --profile ${S3_PROFILE}
exit
exit" >> ${AUX_PRESS}

echo -e "\n#########################################################"
echo -e "###################### JOB STARTED ######################"
echo -e "#########################################################\n"

for SERVER in "${SERVERS[@]}"
	do
		echo -e "\n#########################################################"
		echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Replicating in $SERVER"	
		echo -e "#########################################################\n"

		ssh -tti ${PEM} ${USER}@${SERVER} 'bash -s' < ${AUX_PRESS}
		
		if [ $? -ne 0 ]; then
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] Error during replication in ${SERVER}."
			echo -e "#########################################################\n"
		else
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Replication in ${SERVER} done."
			echo -e "#########################################################\n"
		fi

	done

echo -e "\n#########################################################"
echo -e "##################### JOB COMPLETE ######################"
echo -e "#########################################################\n"

rm -rf ${AUX_PRESS}

exit 0
