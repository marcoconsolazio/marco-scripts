#!/bin/bash

SERVERS_RAW=$1

case "${SERVERS_RAW}" in
	"qa")
		SERVERS=("10.59.6.10" "10.59.6.125")
		;;
	"preprod")
		SERVERS=("10.59.2.13" "10.59.2.70")
		;;
	"prod")
		SERVERS=("10.59.2.10" "10.59.2.90" "10.59.2.170" "10.29.3.116" "10.85.3.161" "10.10.1.80" "10.10.11.139" "10.10.11.146")
		;;
	*)
		echo "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] USAGE: cache_cleaner.sh env"
		exit 1
esac

USER="rundeck"
PEM="/var/lib/rundeck/.ssh/rundeckuser.pem"
CACHE_DIR="/mnt/disk-b/aem/cache/"
AUX_CACHE="aux_cache.sh"

echo "sudo -i
rm -rf /mnt/disk-b/aem/cache/content/st_com/en/products/*
rm -rf /mnt/disk-b/aem/cache/content/st_com/ja/products/*
rm -rf /mnt/disk-b/aem/cache/content/st_com/zh/products/*
rm -rf /mnt/disk-b/aem/cache/etc/*
rm -rf /mnt/disk-b/aem/cache/content/st_com/en/applications/*
rm -rf /mnt/disk-b/aem/cache/content/st_com/ja/applications/*
rm -rf /mnt/disk-b/aem/cache/content/st_com/cn/applications/*
find /mnt/disk-b/aem/cache -name *.html -o -name *.css -o -name *.js -delete
exit
exit" >> ${AUX_CACHE}

echo -e "\n#########################################################"
echo -e "###################### JOB STARTED ######################"
echo -e "#########################################################\n"

for SERVER in "${SERVERS[@]}"
	do
		echo -e "\n#########################################################"
		echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Cleaning cache in $SERVER"	
		echo -e "#########################################################\n"

		ssh -tti ${PEM} ${USER}@${SERVER} 'bash -s' < ${AUX_CACHE}
		
		if [ $? -ne 0 ]; then
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] Error cleaning cache in ${SERVER}."
			echo -e "#########################################################\n"
		else
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Cache in ${SERVER} cleaned."
			echo -e "#########################################################\n"
		fi

	done

rm -rf ${AUX_CACHE}

echo -e "\n#########################################################"
echo -e "###################### JOB FINISHED #####################"
echo -e "#########################################################\n"

exit 0
