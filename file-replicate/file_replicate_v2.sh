#!/bin/bash

##### CONFIGURATIONS #####

SERVERS=("10.59.3.17" "10.59.3.115")
FILE=$1
REPLICATION_DIR=$2
S3_DIR="s3://st-microelectronics-eu-tmp/file-replicate/"
S3_ACCOUNT="stormS3"
PEM="/var/lib/rundeck/.ssh/rundeckuser.pem"
AUX_SCRIPT="aux_script.sh"
DATE=$(date +%Y%m%d_%H%M)
BKP_FILE="${FILE}_bkp_${DATE}"
USER="rundeck"

##### WIP: Append the / after the folder to remove the cd command #####
#i=$((${#REPLICATION_DIR}-1))
#LAST_CHAR=${REPLICATION_DIR:$i:1}
#if [ ${LAST_CHAR} != "/" ]; then
#REPLICATION_DIR="${REPLICATION_DIR}/"
#fi

if [ -z ${FILE} ] || [  -z ${REPLICATION_DIR} ]; then
	echo "File necessario!!"
	exit 1
fi

echo "[INFO]: Job started"

# GENERATE THE AUX_SCRIPT
echo "#!/bin/bash"  >> ${AUX_SCRIPT}
echo "cd ${REPLICATION_DIR}" >> ${AUX_SCRIPT}
echo "cp ${FILE} ${BKP_FILE}" >> ${AUX_SCRIPT}
echo "yes | aws s3 cp ${S3_DIR}${FILE} . --profile ${S3_ACCOUNT}" >> ${AUX_SCRIPT}
echo "apachectl -t" >> ${AUX_SCRIPT}
echo "if [ $? -ne 0 ]; then" >> ${AUX_SCRIPT}
echo "yes | cp ${BKP_FILE} ${FILE}" >> ${AUX_SCRIPT}
echo "echo \"$(date '+%Y-%m-%d %H:%M') [ERROR] Import of ${FILE} failed because an incorrect syntax\"" >> ${AUX_SCRIPT}
echo "else" >> ${AUX_SCRIPT}
echo "rm -rf ${BKP_FILE}" >> ${AUX_SCRIPT}
echo "echo \"$(date '+%Y-%m-%d %H:%M') [INFO] Import of ${FILE} done\"" >> ${AUX_SCRIPT}
echo "fi" >> ${AUX_SCRIPT}	
echo "apachectl graceful" >> ${AUX_SCRIPT}

for SERVER in "${SERVERS[@]}"
	do
		echo "[INFO]: Starting import in $SERVER"
		
		scp -i ${PEM} ${AUX_SCRIPT} ${USER}@${SERVER}:/tmp/${AUX_SCRIPT}
		#ssh -tti ${PEM} ${USER}@${SERVER} << EOF 
		#sudo -i
		#bash /tmp/${AUX_SCRIPT}
		#rm -rf /tmp/${AUX_SCRIPT}" 
		#EOF
	done

rm -rf ${AUX_SCRIPT}

exit 0
