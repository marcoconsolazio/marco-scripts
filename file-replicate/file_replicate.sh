#!/bin/bash

##### CONFIGURATIONS #####

# ARRAY OF EVERY SERVER U WANT TO REPLICATE
server=("123.456.78.90" "90.67.54.250")

# FILES TO REPLICATE IN EVERY SERVER
#files=("405.conf" "406.conf")
	   
##### WIP: FETCH EVERY SYSTEM ARGUMENT AN PUT THEM INTO AN ARRAY: TO USE FOR MODULAR SCRIPT #####
files=("$@")

# DIRECTORY WHERE U WANNA DUPLICATE (WITH / AT THE END)
repl_dir="/etc/httpd/conf.d/redirect/"

# S3 PATH OF YOUR FILE (WITH / AT THE END)
s3_dir="s3://st-microelectronics-eu-tmp/file-replicate/"

# S3 ACCOUNT
s3_acc="stormS3"

# PEM KEY TO CONNECT TO SERVERS
pemKeyPath="rundeck.pem"

# TEMPORARY FILE WITH SCRIPT TO EXECUTE INTO SERVERS
dummy_script_file="dummy_script.sh"

# ACTUAL DATE USED FOR BKP_FILE
now_date=$(date +%Y%m%d_%H%M)

echo "[INFO]: Job started"

# GENERATE THE DUMMY_SCRIPT
echo "#!/bin/bash"  > $dummy_script_file     # START OF A BASH FILE
echo "cd ${repl_dir}" >> $dummy_script_file     # ENTER INTO THE FOLDER WHEN U WANNA REPLICATE
for file in "${files[@]}"
	do
		# BACKUP OF FILE U WANNA REPLICATE
		bkp_file="${file}_bkp_${now_date}"
		
		echo "cp ${file} ${bkp_file}" >> $dummy_script_file     # CREATE A BACKUP OF ACTUAL FILE
		echo "aws s3 cp ${s3_dir}${file} . --profile ${s3_acc}" >> $dummy_script_file     # COPY THE SELECTED FILE FROM AWS S3 TO THE ACTUAL FOLDER
		echo "apachectl -t" >> $dummy_script_file     # CHECK THE APACHE SYNTAX
		echo "if [ $? -ne 0 ]; then" >> $dummy_script_file     # IF THE RETURNED VALUE IS NOT = 0 (INCORRECT SYNTAX)
		echo "cp ${bkp_file} ${file}" >> $dummy_script_file     # RESTORE THE OLD FILE
		echo "echo \"[ERROR]: Import of $file failed because an incorrect syntax\"" >> $dummy_script_file
		echo "else" >> $dummy_script_file     # ELSE (IF THE SYNTAX IS CORRECT)
		echo "rm -rf ${bkp_file}" >> $dummy_script_file     # REMOVE THE BACKUP FILE
		echo "echo \"[INFO]: Import of $file done\"" >> $dummy_script_file
		echo "fi" >> $dummy_script_file	
	done
echo "apachectl graceful" >> $dummy_script_file     # RESTART APACHE WITH GRACEFUL

# FOR EVERY SERVER IN ARRAY DO SOMETHING
for server in "${server[@]}"
	do
		echo "[INFO]: Starting import in $server"
		
		# UPLOAD THE DUMMY_SCRIPT INTO SERVER
		# scp -i $pemKeyPath $dummy_script_file rundeck@$server:$dummy_script_file
		
		# EXECUTE DUMMY_SCRIPT THROUGH SSH WITH SUDO PERMISSIONS AND AFTER DELETE IT
		# ssh -tti $pemKeyPath rundeck@$server "sudo -i bash $dummy_script_file && rm -rf $dummy_script_file"
		
	done

# REMOVE THE DUMMY_SCRIPT FROM LOCAL FOLDER
# rm -rf $dummy_script_file

echo "[INFO]: Job terminated"
exit 0