#!/bin/bash

##### CONFIGURATIONS #####

# ARRAY OF EVERY SERVER U WANT TO REPLICATE
server=("123.456.78.90" 
		"90.67.54.250")

# FILE TO REPLICATE IN EVERY SERVER
file_import="405.conf"

# DIRECTORY WHERE U WANNA DUPLICATE (WITH / AT THE END)
repl_dir="/etc/httpd/conf.d/redirect_custom/"

# S3 PATH OF YOUR FILE (WITH / AT THE END)
s3_dir="s3://st-microelectronics-eu-tmp/redirect_custom/"

# S3 ACCOUNT
s3_acc="stormS3_custom"

# PEM KEY TO CONNECT TO SERVERS
pemKeyPath="rundeck.pem"

# TEMPORARY FILE WITH SCRIPT TO EXECUTE INTO SERVERS
dummy_script_file="dummy_script.sh"

# ACTUAL DATE USED FOR BKP_FILE
now_date=$(date +%Y%m%d_%H%M)

# BACKUP OF FILE U WANNA REPLICATE
bkp_file="${file_import}_bkp_${now_date}"

echo "[INFO]: Job started"

# GENERATE THE DUMMY_SCRIPT
echo "#!/bin/bash"  > $dummy_script_file
echo "cd ${repl_dir}" >> $dummy_script_file 
echo "cp ${file_import} ${bkp_file}" >> $dummy_script_file 
echo "aws s3 cp ${s3_dir}${file_import} . --profile ${s3_acc}" >> $dummy_script_file 
echo "apachectl -t" >> $dummy_script_file
echo "if [ $? -ne 0 ]; then" >> $dummy_script_file
echo "cp ${bkp_file} ${file_import}" >> $dummy_script_file
echo "touch import_failed.err" >> $dummy_script_file
echo "else" >> $dummy_script_file
echo "apachectl graceful" >> $dummy_script_file
echo "rm -rf ${bkp_file}" >> $dummy_script_file
echo "fi" >> $dummy_script_file

# FOR EVERY SERVER IN ARRAY DO SOMETHING
for i in "${server[@]}"
	do
		echo "[INFO]: Starting import in $i"
		
		# UPLOAD THE DUMMY_SCRIPT INTO SERVER
		# scp -i $pemKeyPath $dummy_script_file rundeck@$i:$dummy_script_file
		
		# EXECUTE DUMMY_SCRIPT THROUGH SSH WITH SUDO PERMISSIONS AND AFTER DELETE IT
		# ssh -tti $pemKeyPath rundeck@$i "sudo -i bash $dummy_script_file && rm -rf $dummy_script_file"
		
		# CHECK IF AN ERROR OCCURRED DURING EXECUTION OF DUMMY_SCRIPT
		if [ -a import_failed.err ]; then
			echo "[ERROR]: Import failed in $i because an incorrect syntax"
			rm -rf import_failed.err
		else
			echo "[INFO]: Import on $i done."
		fi
		
	done

# REMOVE THE DUMMY_SCRIPT FROM LOCAL FOLDER
# rm -rf $dummy_script_file

echo "[INFO]: Job terminated"
exit 0

