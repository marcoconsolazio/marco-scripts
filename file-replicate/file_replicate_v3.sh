#!/bin/bash

SERVERS_RAW=$3
FILES_RAW=$1

case "${SERVERS_RAW}" in
	"all_prod_dispatcher")
		SERVERS=("10.59.3.12" "10.59.3.113" "10.59.2.188" "10.29.2.225" "10.85.2.39" "10.10.2.34" "10.10.3.14" "10.10.3.54")
		;;
	"dev_env")
		SERVERS=("10.59.9.70" "10.59.9.69")
		;;
	"all_preprod_dispatcher")
		SERVERS=("10.59.2.13" "10.59.2.70")
		;;
	*)
		IFS=',' read -r -a SERVERS <<< "$SERVERS_RAW"
		;;
esac

IFS=',' read -r -a FILES <<< "$FILES_RAW"

#FILE=$1
REPLICATION_DIR=$2
S3_DIR="s3://st-microelectronics-eu-tmp/file-replicate/"
S3_BKP="${S3_DIR}backups/"
S3_ACCOUNT="stormS3"
PEM="/var/lib/rundeck/.ssh/rundeckuser.pem"
AUX_SCRIPT="aux_script.sh"
DATE=$(date +%Y%m%d_%H%M)
USER="rundeck"

echo -e "\n#########################################################"
echo -e "###################### JOB STARTED ######################"
echo -e "#########################################################\n"

# GENERATE THE AUX_SCRIPT
echo "sudo -i" >> ${AUX_SCRIPT}
echo "cd ${REPLICATION_DIR}" >> ${AUX_SCRIPT}

FILES_FOUND=0
for FILE in "${FILES[@]}"
	do
		BKP_FILE="${FILE}_bkp_${DATE}"

		aws s3 ls ${S3_DIR}${FILE}
		if [ $? -ne 0 ]; then
			echo -e "$(date '+%Y-%m-%d %H:%M:%S')\t The file ${FILE} doesn't exists in S3."
			continue
		else
			FILES_FOUND=1
		fi

		echo "cp ${FILE} ${BKP_FILE}" >> ${AUX_SCRIPT}
		echo "aws s3 cp ${BKP_FILE} ${S3_BKP} --profile ${S3_ACCOUNT}" >> ${AUX_SCRIPT}
		echo "yes | aws s3 cp ${S3_DIR}${FILE} . --profile ${S3_ACCOUNT}" >> ${AUX_SCRIPT}
	done
if [ ${FILES_FOUND} -ne 1 ]; then
	exit 1
fi

echo "apachectl graceful" >> ${AUX_SCRIPT}
echo "exit" >> ${AUX_SCRIPT}
echo "exit" >> ${AUX_SCRIPT}

for SERVER in "${SERVERS[@]}"
	do
		echo -e "\n#########################################################"
		echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Starting import in $SERVER"	
		echo -e "#########################################################\n"

		ssh -tti ${PEM} ${USER}@${SERVER} 'bash -s' < ${AUX_SCRIPT}
		if [ $? -ne 0 ]; then
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [ERROR] Error during replication in ${SERVER}."
			echo -e "#########################################################\n"
		else
			echo -e "\n#########################################################"
			echo -e "$(date '+%Y-%m-%d %H:%M:%S') [INFO] Import in ${SERVER} done."
			echo -e "#########################################################\n"
		fi

	done

echo -e "\n#########################################################"
echo -e "##################### JOB COMPLETE ######################"
echo -e "#########################################################\n"

rm -rf ${AUX_SCRIPT}

exit 0
