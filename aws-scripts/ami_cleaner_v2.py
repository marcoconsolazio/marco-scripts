# /usr/bin/python2.7
import time
from datetime import datetime, timedelta
import boto.ec2
import sys

# specify AWS keys
auth = {"aws_access_key_id": "AKIAIIEWDD6CLICMYM2Q", "aws_secret_access_key": "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"}
ec2_region = 'eu-west-1'
retention_days = 0 #180
owner = 344288453045

def connectAWS():

    try:
        ec2 = boto.ec2.connect_to_region(ec2_region, **auth)
        print ('Connected to AWS successfully')
    except Exception, e1:
        error1 = "Error1: " + str(e1)
        print(error1)
        sys.exit(0)

    return ec2

def listAMI():
	
    ec2 = connectAWS()
    #images = ec2.get_all_images(owners=[owner])
    images = ec2.get_all_images(image_ids=["ami-f34a7d95","ami-1d4e797b","ami-6b4c7b0d"])
    delete_time = datetime.utcnow() - timedelta(days=retention_days)

    for image in images:
        creation_time = datetime.strptime(image.creationDate, '%Y-%m-%dT%H:%M:%S.000Z')
        if creation_time < delete_time:
	    ami_id = image.id
            deleteAMI(ec2,ami_id)
	    print ("\n")
	
def deleteAMI(ec2,ami_id):

    snap_to_delete = []

    ami = ec2.get_image(image_id=ami_id)
    print ("AMI DO TELETE: " + str(ami_id) + " - Creation: " + str(ami.creationDate))
    blockDevice = ami.block_device_mapping
	
    for volume in blockDevice:
        snap = str(ami.block_device_mapping[volume].snapshot_id)
	if snap != "None":
            snap_to_delete.append(snap)
	    print ("-> SNAP TO DELETE: " + snap)	
   
    deregistered = ami.deregister()
    if (deregistered):
        print ("Deregistering...")

	while (ec2.get_image(image_id=ami_id)):
            ami = ec2.get_image(image_id=ami_id)
            #print ("STATUS: " + str(ami.state))
            time.sleep(3)

	print ("Succesfully deregistered " + ami_id)

	for snapshot in snap_to_delete:
            deleted = ec2.delete_snapshot(snapshot)
            if (deleted):
                print ("Snapshot " + snapshot + " deleted")
            else:
                print ("Error during deleting of " + snapshot)

    else:
        print ("Error during deregistering of " + ami_id)


if __name__ == '__main__':
    listAMI()
