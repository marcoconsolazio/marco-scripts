#!/bin/bash

########################################################
## For Documentation see these tickets:
## https://mantis.stormreply.com/view.php?id=14355
## https://jira-st.stormreply.com/jira/browse/STM-6103
## Docs: https://docs.google.com/document/d/1xloSjyZOECIGqM6-jgOEM5rKNJGAirl0bGubjX8DnC8
########################################################

USER=rundeck
PEM=/var/lib/rundeck/.ssh/rundeckuser.pem
SERVER=10.59.8.75
SCRIPT=/usr/src/script/qa_export_daily.sh

echo "$(date '+%Y-%m-%d %H:%M') - INFO - Starting Export package: ${PACKAGE}" >&2

# Connect SSH to Solr02 and run the script
ssh -tti ${PEM} ${USER}@${SERVER} "sudo -i bash ${SCRIPT}"
if [ $? -ne 0 ]; then
    echo "$(date '+%Y-%m-%d %H:%M') - ERROR - Cannot SSH to ${SOLR}" >&2
    exit 1
fi
