#!/bin/bash

########################################################
## For Documentation see these tickets:
## https://mantis.stormreply.com/view.php?id=14355
## https://jira-st.stormreply.com/jira/browse/STM-6103
## Docs: https://docs.google.com/document/d/1xloSjyZOECIGqM6-jgOEM5rKNJGAirl0bGubjX8DnC8
########################################################

PUBLISH=10.59.3.57
PACKAGE=export-daily-preprod-01.zip
ADMIN_PWD=LLH4DkF0im8cfrYNjAcuDFJi21YjgbRd
FILE=export_daily-$(date +'%Y-%m-%d').zip

echo "$(date '+%Y-%m-%d %H:%M') - INFO - deleting all the files from /opt/import" >&2
## Moving to the folder /opt/import
cd /opt/import
## Removing all the files inside /opt/import
rm -rf *

## Building the AEM package export_daily-v01.zip 
curl -u admin:${ADMIN_PWD} -v -X POST http://${PUBLISH}:4503/crx/packmgr/service/.json/etc/packages/my_packages/${PACKAGE}?cmd=build
ret=$?
if [ $ret -ne 0 ]; then
    echo "$(date '+%Y-%m-%d %H:%M') -  ERROR - Building package, curl returned: $ret" >&2
    exit $ret
else
	echo "$(date '+%Y-%m-%d %H:%M') -  INFO - Package build succesfully" >&2
fi

## Download the package 
curl -u admin:${ADMIN_PWD} http://${PUBLISH}:4503/etc/packages/my_packages/${PACKAGE} > ${FILE}
ret=$?
if [ $ret -ne 0 ]; then
    echo "$(date '+%Y-%m-%d %H:%M') -  ERROR - Downloading package failed, curl returned: $ret" >&2
    exit $ret
else
	echo "$(date '+%Y-%m-%d %H:%M') -  INFO - Package succesfully doqnloaded" >&2
fi

echo "$(date '+%Y-%m-%d %H:%M') - INFO - Unzipping the zip file in the export directory" >&2

## Creating the export folder
mkdir export

## Unzipping the package inside the folder export
cd export
unzip -q ../${FILE}
echo "$(date '+%Y-%m-%d %H:%M') - INFO - Unzip DONE" >&2

## Remove the package zip file
echo "$(date '+%Y-%m-%d %H:%M') - INFO - Removing the zip file" >&2
cd ..
rm -f ${FILE}

## Execute the php script (owner - Zitec)
echo "$(date '+%Y-%m-%d %H:%M') - INFO - Running products_import_cron.php " >&2
#php /home/vrosca/st_scripts/st_import/products_import_cron.php > /home/vrosca/st_scripts/st_import/logs/$(date +'%Y-%m-%d')_general_log.txt 2>&1

echo "$(date '+%Y-%m-%d %H:%M') - INFO - END SCRIPT" >&2

exit
